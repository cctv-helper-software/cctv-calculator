unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TMainForm }

  TMainForm = class(TForm)
    RunCamerasBadwidthCalculatorButton: TButton;
    procedure RunCamerasBadwidthCalculatorButtonClick(Sender: TObject);
  private

  public

  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.RunCamerasBadwidthCalculatorButtonClick(Sender: TObject);
begin
  SysUtils.ExecuteProcess('./cameras_bandwidth_calculator_gui.exe', '', []);
end;

end.


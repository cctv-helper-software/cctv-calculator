-- Resolution of the camera.
-- Correct values:
-- * QCIF (176x120 px)
-- * CIF (352x240 px)
-- * 4CIF (704x480 px)
-- * D1 (720x480 px)
-- * 1 Mpix (1280x720 px)
-- * 2 Mpix (1920x1080 px)
-- * 3 Mpix (2048x1536 px)
-- * 4 Mpix (2560x1440 px)
-- * 5 Mpix (2592x1944 px)
-- * 6 Mpix (3072x2048 px)
-- * 8 Mpix (3840x2160 px)
-- * 12 Mpix (4000x3000 px)
-- * 16 Mpix (4592x3056 px)
-- Default value: 2 Mpix (1920x1080 px)
Resolution = "2 Mpix (1920x1080 px)"

-- Quality of the image.
-- Correct values:
-- * Low
-- * Medium
-- * High
-- Default value: Medium
Quality = "Medium"

-- Compression algorithm used.
-- Correct values:
-- * h.265+
-- * h.265
-- * h.264
-- * MPEG-4
-- * MPEG-2
-- * MJPEG
-- Default value: h.264
Compression = "h.264"

-- Frames per second.
-- Has to be entered as positive integer.
-- Default value: 15
FPS = 15

-- Number of cameras.
-- Has to be entered as positive integer.
Cameras = 1
